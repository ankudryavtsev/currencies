package me.akudryavtsev.currencies.domain.converter;

import org.junit.Test;

import me.akudryavtsev.currencies.model.domain.Currency;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class CurrencyConverterTest {

    @Test
    public void fromString() {
        final String string = "USD";
        final Currency actual = CurrencyConverter.fromString(string);
        final Currency expected = Currency.USD;
        assertThat(actual, is(expected));
    }

    @Test
    public void fromCurrency() {
        final Currency currency = Currency.USD;
        final String actual = CurrencyConverter.fromCurrency(currency);
        final String expected = "USD";
        assertThat(actual, is(expected));
    }
}