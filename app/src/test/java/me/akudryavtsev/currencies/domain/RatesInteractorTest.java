package me.akudryavtsev.currencies.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import io.reactivex.Single;
import me.akudryavtsev.currencies.domain.converter.RatesConverter;
import me.akudryavtsev.currencies.domain.repository.IRatesRepository;
import me.akudryavtsev.currencies.model.data.RatesResponse;
import me.akudryavtsev.currencies.model.domain.Currency;
import me.akudryavtsev.currencies.model.domain.Rates;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RatesInteractorTest {

    @Mock
    private IRatesRepository ratesRepository;

    private RatesInteractor ratesInteractor;

    @Before
    public void setUp() {
        ratesInteractor = new RatesInteractor(ratesRepository);
    }

    @Test
    public void getCurrentRates() {
        final RatesResponse ratesResponse = new RatesResponse();
        final String baseCurrency = "USD";
        ratesResponse.setBaseCurrency(baseCurrency);
        ratesResponse.setRates(Collections.emptyMap());
        when(ratesRepository.getLatestRates(baseCurrency)).thenReturn(Single.just(ratesResponse));
        final Rates actual = ratesInteractor.getCurrentRates(Currency.USD).blockingGet();
        final Rates rates = RatesConverter.toRates(ratesResponse);
        assertThat(actual, is(rates));
    }
}