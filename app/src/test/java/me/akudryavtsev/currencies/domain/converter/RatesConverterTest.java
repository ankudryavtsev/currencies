package me.akudryavtsev.currencies.domain.converter;

import org.junit.Test;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import me.akudryavtsev.currencies.model.data.RatesResponse;
import me.akudryavtsev.currencies.model.domain.Currency;
import me.akudryavtsev.currencies.model.domain.Rates;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class RatesConverterTest {

    @Test
    public void toRates() {
        final RatesResponse ratesResponse = getRatesResponse();
        final Rates rates = RatesConverter.toRates(ratesResponse);
        assertThat(rates.getBaseCurrency(), is(Currency.USD));
        assertThat(rates.getDate(), is(getDate()));
        assertThat(rates.getRates().size(), is(2));
        assertThat(rates.getRates().get(Currency.EUR), is(34.34));
        assertThat(rates.getRates().get(Currency.RUB), is(323.23));
    }

    private RatesResponse getRatesResponse() {
        final RatesResponse ratesResponse = new RatesResponse();
        ratesResponse.setBaseCurrency("USD");
        ratesResponse.setDate(getDate());
        final HashMap<String, Double> ratesMap = getRatesMap();
        ratesResponse.setRates(ratesMap);
        return ratesResponse;
    }

    private Date getDate() {
        final GregorianCalendar calendar = new GregorianCalendar(2018, 4, 26);
        return calendar.getTime();
    }

    private HashMap<String, Double> getRatesMap() {
        final HashMap<String, Double> ratesMap = new HashMap<>();
        ratesMap.put("EUR", 34.34);
        ratesMap.put("RUB", 323.23);
        return ratesMap;
    }

}