package me.akudryavtsev.currencies.model;

import com.google.gson.Gson;

import org.junit.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;

import me.akudryavtsev.currencies.model.data.RatesResponse;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class RatesResponseTest {

    private static final String SOURCE = "{\"base\":\"EUR\",\"date\":\"2018-09-06\",\"rates\":" +
            "{\"AUD\":1.6219,\"BGN\":1.9625,\"BRL\":4.8082,\"CAD\":1.539,\"CHF\":1.1313,\"CNY\":7.9722," +
            "\"CZK\":25.803,\"DKK\":7.4822,\"GBP\":0.90131,\"HKD\":9.1636,\"HRK\":7.4595,\"HUF\":327.6," +
            "\"IDR\":17383.0,\"ILS\":4.1848,\"INR\":84.003,\"ISK\":128.24,\"JPY\":129.99,\"KRW\":1309.2," +
            "\"MXN\":22.442,\"MYR\":4.8284,\"NOK\":9.8094,\"NZD\":1.7693,\"PHP\":62.806,\"PLN\":4.333," +
            "\"RON\":4.6543,\"RUB\":79.846,\"SEK\":10.627,\"SGD\":1.6055,\"THB\":38.26,\"TRY\":7.6542," +
            "\"USD\":1.1674,\"ZAR\":17.884}}";

    @Test
    public void parse() {
        final Gson gson = new Gson();
        final RatesResponse ratesResponse = gson.fromJson(SOURCE, RatesResponse.class);
        assertThat(ratesResponse.getBaseCurrency(), is("EUR"));
        final GregorianCalendar calendar = new GregorianCalendar(2018, Calendar.SEPTEMBER, 6);
        assertThat(ratesResponse.getDate(), is(calendar.getTime()));
        assertThat(ratesResponse.getRates().size(), is(32));
        assertThat(ratesResponse.getRates().get("BRL"), is(4.8082));
    }
}