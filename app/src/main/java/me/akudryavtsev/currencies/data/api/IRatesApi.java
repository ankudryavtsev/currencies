package me.akudryavtsev.currencies.data.api;

import io.reactivex.Single;
import me.akudryavtsev.currencies.model.data.RatesResponse;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IRatesApi {
    @GET("latest")
    Single<RatesResponse> getLatestRates(@Query("base") String baseCurrency);
}
