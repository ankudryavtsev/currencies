package me.akudryavtsev.currencies.data.repository;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import io.reactivex.Single;
import me.akudryavtsev.currencies.data.api.IRatesApi;
import me.akudryavtsev.currencies.domain.repository.IRatesRepository;
import me.akudryavtsev.currencies.model.data.RatesResponse;

public class RatesRepository implements IRatesRepository {

    private final IRatesApi ratesApi;

    @Inject
    public RatesRepository(@NonNull IRatesApi ratesApi) {
        this.ratesApi = ratesApi;
    }

    @NonNull
    @Override
    public Single<RatesResponse> getLatestRates(@NonNull String baseCurrency) {
        return ratesApi.getLatestRates(baseCurrency);
    }
}
