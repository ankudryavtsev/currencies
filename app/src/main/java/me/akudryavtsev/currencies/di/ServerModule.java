package me.akudryavtsev.currencies.di;

import android.support.annotation.NonNull;

import me.akudryavtsev.currencies.data.api.IRatesApi;
import me.akudryavtsev.currencies.data.repository.RatesRepository;
import me.akudryavtsev.currencies.di.provider.OkHttpClientProvider;
import me.akudryavtsev.currencies.di.provider.RatesApiProvider;
import me.akudryavtsev.currencies.di.qualifier.ServerPath;
import me.akudryavtsev.currencies.domain.RatesInteractor;
import me.akudryavtsev.currencies.domain.repository.IRatesRepository;
import okhttp3.OkHttpClient;
import toothpick.config.Module;

public class ServerModule extends Module {

    public ServerModule(@NonNull String serverUrl) {
        bind(String.class).withName(ServerPath.class).toInstance(serverUrl);
        bind(OkHttpClient.class).toProvider(OkHttpClientProvider.class).providesSingletonInScope();
        bind(IRatesApi.class).toProvider(RatesApiProvider.class).providesSingletonInScope();
        bind(IRatesRepository.class).to(RatesRepository.class).singletonInScope();
        bind(RatesInteractor.class).singletonInScope();
    }
}
