package me.akudryavtsev.currencies.di.provider;

import android.support.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Provider;

import me.akudryavtsev.currencies.data.api.IRatesApi;
import me.akudryavtsev.currencies.di.qualifier.ServerPath;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RatesApiProvider implements Provider<IRatesApi> {

    private final String serverPath;
    private final OkHttpClient okHttpClient;

    @Inject
    public RatesApiProvider(@ServerPath @NonNull String serverPath, @NonNull OkHttpClient okHttpClient) {
        this.serverPath = serverPath;
        this.okHttpClient = okHttpClient;
    }

    @Override
    public IRatesApi get() {
        return new Retrofit.Builder()
                .baseUrl(serverPath)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(IRatesApi.class);
    }
}
