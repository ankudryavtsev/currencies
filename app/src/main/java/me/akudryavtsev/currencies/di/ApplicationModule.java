package me.akudryavtsev.currencies.di;

import me.akudryavtsev.currencies.rx.IRxSchedulers;
import me.akudryavtsev.currencies.rx.RxSchedulers;
import toothpick.config.Module;

public class ApplicationModule extends Module {
    public ApplicationModule() {
        bind(IRxSchedulers.class).toInstance(new RxSchedulers());
    }
}
