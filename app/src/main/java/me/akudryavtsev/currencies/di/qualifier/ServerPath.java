package me.akudryavtsev.currencies.di.qualifier;

import javax.inject.Qualifier;

@Qualifier
public @interface ServerPath {
}
