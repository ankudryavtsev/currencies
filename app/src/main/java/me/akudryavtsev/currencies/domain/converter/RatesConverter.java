package me.akudryavtsev.currencies.domain.converter;

import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import me.akudryavtsev.currencies.model.data.RatesResponse;
import me.akudryavtsev.currencies.model.domain.Currency;
import me.akudryavtsev.currencies.model.domain.Rates;

public class RatesConverter {
    public static Rates toRates(@NonNull RatesResponse ratesResponse) {
        final Rates rates = new Rates();
        rates.setBaseCurrency(CurrencyConverter.fromString(ratesResponse.getBaseCurrency()));
        rates.setDate(ratesResponse.getDate());
        rates.setRates(convertRates(ratesResponse.getRates()));
        return rates;
    }

    private static Map<Currency, Double> convertRates(@NonNull Map<String, Double> rates) {
        final Map<Currency, Double> convertedRates = new HashMap<>();
        for (Map.Entry<String, Double> entry : rates.entrySet()) {
            convertedRates.put(CurrencyConverter.fromString(entry.getKey()), entry.getValue());
        }
        return convertedRates;
    }
}
