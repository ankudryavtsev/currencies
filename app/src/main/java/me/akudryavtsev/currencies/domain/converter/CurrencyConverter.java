package me.akudryavtsev.currencies.domain.converter;

import android.support.annotation.NonNull;

import me.akudryavtsev.currencies.model.domain.Currency;

public class CurrencyConverter {
    public static Currency fromString(@NonNull String currencyString) {
        return Currency.valueOf(currencyString);
    }

    public static String fromCurrency(@NonNull Currency currency) {
        return currency.name();
    }
}
