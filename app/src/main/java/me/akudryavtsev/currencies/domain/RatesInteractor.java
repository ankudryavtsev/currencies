package me.akudryavtsev.currencies.domain;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import io.reactivex.Single;
import me.akudryavtsev.currencies.domain.converter.CurrencyConverter;
import me.akudryavtsev.currencies.domain.converter.RatesConverter;
import me.akudryavtsev.currencies.domain.repository.IRatesRepository;
import me.akudryavtsev.currencies.model.domain.Currency;
import me.akudryavtsev.currencies.model.domain.Rates;

public class RatesInteractor {

    private IRatesRepository ratesRepository;

    @Inject
    public RatesInteractor(@NonNull IRatesRepository ratesRepository) {
        this.ratesRepository = ratesRepository;
    }

    public Single<Rates> getCurrentRates(@NonNull Currency baseCurrency) {
        return ratesRepository.getLatestRates(CurrencyConverter.fromCurrency(baseCurrency))
                .map(RatesConverter::toRates);
    }
}
