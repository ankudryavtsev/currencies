package me.akudryavtsev.currencies.domain.repository;

import android.support.annotation.NonNull;

import io.reactivex.Single;
import me.akudryavtsev.currencies.model.data.RatesResponse;

public interface IRatesRepository {
    @NonNull
    Single<RatesResponse> getLatestRates(@NonNull String baseCurrency);
}
