package me.akudryavtsev.currencies.presentation.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import me.akudryavtsev.currencies.R;
import me.akudryavtsev.currencies.di.ScopeNames;
import me.akudryavtsev.currencies.presentation.presenter.MainPresenter;
import toothpick.Toothpick;

public class MainActivity extends AppCompatActivity implements IMainView {

    @Inject
    MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toothpick.openScope(ScopeNames.APPLICATION_SCOPE);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mainPresenter.bindView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mainPresenter.unbindView();
    }
}
