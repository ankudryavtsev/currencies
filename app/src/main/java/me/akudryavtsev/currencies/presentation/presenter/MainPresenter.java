package me.akudryavtsev.currencies.presentation.presenter;

import android.support.annotation.NonNull;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import me.akudryavtsev.currencies.domain.RatesInteractor;
import me.akudryavtsev.currencies.model.domain.Currency;
import me.akudryavtsev.currencies.model.domain.Rates;
import me.akudryavtsev.currencies.model.presentation.Value;
import me.akudryavtsev.currencies.presentation.view.IMainView;
import me.akudryavtsev.currencies.rx.IRxSchedulers;

public class MainPresenter {

    private final RatesInteractor ratesInteractor;
    private final IRxSchedulers rxSchedulers;

    private IMainView mainView;
    private Disposable disposable;

    @Inject
    public MainPresenter(@NonNull RatesInteractor ratesInteractor,
                         @NonNull IRxSchedulers rxSchedulers) {
        this.ratesInteractor = ratesInteractor;
        this.rxSchedulers = rxSchedulers;
    }

    public void bindView(IMainView mainView) {
        this.mainView = mainView;
    }

    public void unbindView() {
        mainView = null;
        if (disposable != null) {
            disposable.dispose();
        }
    }

    public void updateValues(@NonNull Currency baseCurrency, double newValue) {
        disposable = ratesInteractor.getCurrentRates(baseCurrency)
                .map(rates -> mapEntrySet(rates, newValue))
                .map(list -> {
                    list.add(0, new Value(baseCurrency, newValue));
                    return list;
                })
                .subscribeOn(rxSchedulers.getIOScheduler())
                .observeOn(rxSchedulers.getMainThreadScheduler())
                .subscribe(this::onUpdated, this::onError);
    }

    private void onError(Throwable throwable) {

    }

    private void onUpdated(List<Value> values) {

    }

    private List<Value> mapEntrySet(Rates rates, double newValue) {
        final LinkedList<Value> values = new LinkedList<>();
        for (Map.Entry<Currency, Double> entry : rates.getRates().entrySet()) {
            values.add(new Value(entry.getKey(), entry.getValue() * newValue));
        }
        return values;
    }
}
