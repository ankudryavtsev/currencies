package me.akudryavtsev.currencies;

import android.app.Application;

import me.akudryavtsev.currencies.di.ApplicationModule;
import me.akudryavtsev.currencies.di.ScopeNames;
import me.akudryavtsev.currencies.di.ServerModule;
import toothpick.Scope;
import toothpick.Toothpick;

public class RatesApplication extends Application {

    private static final String SERVER_URL = "https://revolut.duckdns.org";

    @Override
    public void onCreate() {
        super.onCreate();
        final Scope applicationScope = Toothpick.openScope(ScopeNames.APPLICATION_SCOPE);
        applicationScope.installModules(new ServerModule(SERVER_URL), new ApplicationModule());
    }
}
