package me.akudryavtsev.currencies.rx;

import io.reactivex.Scheduler;

public interface IRxSchedulers {
    Scheduler getIOScheduler();

    Scheduler getMainThreadScheduler();

    Scheduler getComputationScheduler();
}
