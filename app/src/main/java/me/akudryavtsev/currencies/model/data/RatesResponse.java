package me.akudryavtsev.currencies.model.data;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.Map;

public class RatesResponse {
    @SerializedName("base")
    private String baseCurrency;

    @SerializedName("date")
    private Date date;

    @SerializedName("rates")
    private Map<String, Double> rates;

    public String getBaseCurrency() {
        return baseCurrency;
    }

    public Date getDate() {
        return date;
    }

    public Map<String, Double> getRates() {
        return rates;
    }

    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setRates(Map<String, Double> rates) {
        this.rates = rates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RatesResponse that = (RatesResponse) o;

        if (baseCurrency != null ? !baseCurrency.equals(that.baseCurrency) : that.baseCurrency != null)
            return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        return rates != null ? rates.equals(that.rates) : that.rates == null;
    }

    @Override
    public int hashCode() {
        int result = baseCurrency != null ? baseCurrency.hashCode() : 0;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (rates != null ? rates.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RatesResponse{" +
                "baseCurrency='" + baseCurrency + '\'' +
                ", date=" + date +
                ", rates=" + rates +
                '}';
    }
}
