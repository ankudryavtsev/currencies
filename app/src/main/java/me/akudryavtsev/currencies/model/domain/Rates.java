package me.akudryavtsev.currencies.model.domain;

import java.util.Date;
import java.util.Map;

public class Rates {
    private Currency baseCurrency;
    private Date date;
    private Map<Currency, Double> rates;

    public Currency getBaseCurrency() {
        return baseCurrency;
    }

    public Date getDate() {
        return date;
    }

    public Map<Currency, Double> getRates() {
        return rates;
    }

    public void setBaseCurrency(Currency baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setRates(Map<Currency, Double> rates) {
        this.rates = rates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rates rates1 = (Rates) o;

        if (baseCurrency != rates1.baseCurrency) return false;
        if (date != null ? !date.equals(rates1.date) : rates1.date != null) return false;
        return rates != null ? rates.equals(rates1.rates) : rates1.rates == null;
    }

    @Override
    public int hashCode() {
        int result = baseCurrency != null ? baseCurrency.hashCode() : 0;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (rates != null ? rates.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Rates{" +
                "baseCurrency=" + baseCurrency +
                ", date=" + date +
                ", rates=" + rates +
                '}';
    }
}
