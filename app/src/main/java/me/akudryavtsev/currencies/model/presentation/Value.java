package me.akudryavtsev.currencies.model.presentation;

import android.support.annotation.NonNull;

import me.akudryavtsev.currencies.model.domain.Currency;

public class Value {
    private Currency currency;
    private double sum;

    public Value(Currency currency, double sum) {
        this.currency = currency;
        this.sum = sum;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(@NonNull Currency currency) {
        this.currency = currency;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Value value = (Value) o;

        if (Double.compare(value.sum, sum) != 0) return false;
        return currency == value.currency;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = currency.hashCode();
        temp = Double.doubleToLongBits(sum);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
